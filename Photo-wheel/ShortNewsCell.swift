//
//  ShortNewsCell.swift
//  Photo-wheel
//
//  Created by Samat Murzaliev on 01.08.2022.
//

import UIKit
import SnapKit

class ShortNewsCell: UICollectionViewCell {
    
    private lazy var newsBodyLabel: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 12, weight: .regular)
        view.textColor = .black
        view.textAlignment = .center
        return view
    }()
    
    private lazy var sourceLabel: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 12, weight: .regular)
        view.textColor = .gray
        view.textAlignment = .right
        return view
    }()
    
    override func layoutSubviews() {
        setupSubview()
    }
    
    private func setupSubview() {
        addSubview(newsBodyLabel)
        newsBodyLabel.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
            make.top.equalToSuperview()
            make.bottom.equalToSuperview().offset(-32)
        }
        
        addSubview(sourceLabel)
        sourceLabel.snp.makeConstraints { make in
            make.bottom.equalToSuperview().offset(-8)
            make.right.equalToSuperview().offset(-16)
        }
    }
    
    func fill(news: String, source: String) {
        newsBodyLabel.text = news
        sourceLabel.text = source
    }
}
