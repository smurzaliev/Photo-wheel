//
//  PhotoWheelCell.swift
//  Photo-wheel
//
//  Created by Samat Murzaliev on 01.08.2022.
//

import UIKit
import SnapKit
import Kingfisher

class PhotoWheelCell: UICollectionViewCell {
    
    private lazy var image: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFit
        return view
    }()
    
    override func layoutSubviews() {
        setupSubview()
    }
    
    private func setupSubview() {
        addSubview(image)
        image.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
    
    func fill(image: UIImage) {
        DispatchQueue.main.async {
            self.image.image = image

        }
    }
}
