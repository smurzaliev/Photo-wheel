//
//  ViewController.swift
//  Photo-wheel
//
//  Created by Samat Murzaliev on 01.08.2022.
//

import UIKit
import SnapKit
import Kingfisher

class ViewController: UIViewController {
    
    private lazy var imageWheel: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0
        let view = UICollectionView(frame: .zero, collectionViewLayout: layout)
        view.allowsSelection = false
        view.register(PhotoWheelCell.self, forCellWithReuseIdentifier: "PhotoWheelCell")
        view.delegate = self
        view.dataSource = self
        view.isPagingEnabled = true
        view.showsHorizontalScrollIndicator = false
        return view
    }()
    
    private lazy var numberStack: UIStackView = {
        let view = UIStackView()
        view.axis = .horizontal
        return view
    }()
    
    private lazy var previousButton: UIButton = {
        let view = UIButton(type: .system)
        view.setImage(UIImage(named: "chevron.left"), for: .normal)
        view.tintColor = UIColor(named: "mainColor")
        view.tag = 10
        view.addTarget(self, action: #selector(imageOrderChange(view:)), for: .touchUpInside)
        return view
    }()
    
    private lazy var nextButton: UIButton = {
        let view = UIButton(type: .system)
        view.setImage(UIImage(named: "chevron.right"), for: .normal)
        view.contentMode = .scaleAspectFit
        view.tintColor = UIColor(named: "mainColor")
        view.tag = 20
        view.addTarget(self, action: #selector(imageOrderChange(view:)), for: .touchUpInside)
        return view
    }()
    
    private lazy var newsBodyLabel: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 12, weight: .regular)
        view.textColor = .black
        view.textAlignment = .center
        view.lineBreakMode = .byWordWrapping
        view.numberOfLines = 0
        return view
    }()
    
    private lazy var sourceLabel: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 12, weight: .regular)
        view.textColor = .gray
        view.textAlignment = .right
        view.numberOfLines = 0
        return view
    }()
    
    private var currentImageIndex = 1
    
    private var newsItems: [(String, String)] = [
        ("В Токмаке следователь напал на девушку в здании РОВД. Ознакомьтесь с тщательно подобранными интерактивными.", "Автор: Иван Иванов  Источник: KaktusMedia"),
        ("Bla Bla Bla Bla","by BBC"),
        ("Ho Ho Ho Ho", "by CNBC"),
        ("В Токмаке следователь напал на девушку в здании РОВД. Ознакомьтесь с тщательно подобранными интерактивными.", "Автор: Иван Иванов  Источник: KaktusMedia"),
        ("Bla Bla Bla Bla","by BBC"),
        ("Ho Ho Ho Ho", "by CNBC"),
        ("В Токмаке следователь напал на девушку в здании РОВД. Ознакомьтесь с тщательно подобранными интерактивными.", "Автор: Иван Иванов  Источник: KaktusMedia"),
        ("Bla Bla Bla Bla","by BBC"),
        ("Ho Ho Ho Ho", "by CNBC"),
        ("В Токмаке следователь напал на девушку в здании РОВД. Ознакомьтесь с тщательно подобранными интерактивными.", "Автор: Иван Иванов  Источник: KaktusMedia")
        ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addButtons()
        setupSubview()
    }
    
    private func setupSubview() {
        view.addSubview(imageWheel)
        imageWheel.snp.makeConstraints { make in
            make.left.right.equalToSuperview()
            make.top.equalToSuperview().offset(50)
            make.height.equalTo(230)
        }
        
        view.addSubview(previousButton)
        previousButton.snp.makeConstraints { make in
            make.width.height.equalTo(24)
            make.left.equalToSuperview().offset(24)
            make.top.equalTo(imageWheel.snp.bottom).offset(20)
        }
        
        view.addSubview(nextButton)
        nextButton.snp.makeConstraints { make in
            make.width.height.equalTo(24)
            make.right.equalToSuperview().offset(-24)
            make.top.equalTo(imageWheel.snp.bottom).offset(20)
        }
        
        view.addSubview(numberStack)
        numberStack.snp.makeConstraints { make in
            make.top.equalTo(imageWheel.snp.bottom).offset(14)
            make.centerX.equalToSuperview()
        }
        
        view.addSubview(newsBodyLabel)
        newsBodyLabel.text = newsItems.first?.0
        newsBodyLabel.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
            make.top.equalTo(numberStack.snp.bottom).offset(10)
            make.height.equalTo(50)
        }
        
        view.addSubview(sourceLabel)
        sourceLabel.text = newsItems.first?.1
        sourceLabel.snp.makeConstraints { make in
            make.top.equalTo(newsBodyLabel.snp.bottom).offset(8)
            make.right.equalToSuperview().offset(-16)
        }
    }
    
    private func addButtons() {
        for i in 1...10 {
            let newButton = UIButton(type: .system)
            newButton.setTitle("\(i)", for: .normal)
            newButton.titleLabel?.font = .systemFont(ofSize: 20, weight: .bold)
            newButton.tintColor = UIColor(named: "diselectColor")
            newButton.addTarget(self, action: #selector(buttonClicked(view:)), for: .touchUpInside)
            numberStack.addArrangedSubview(newButton)
        }
        numberStack.arrangedSubviews.first?.tintColor = UIColor(named: "mainColor")
    }
    
    @objc func imageOrderChange(view: UIButton) {
        if view.tag == 10 {
            if currentImageIndex >= 2 { currentImageIndex -= 1 }
        } else if view.tag == 20 {
            if currentImageIndex <= 9 { currentImageIndex += 1 }
        }
        
        let indexPath = IndexPath(item: currentImageIndex - 1, section: 0)
        DispatchQueue.main.async {
            self.imageWheel.isPagingEnabled = false
            self.imageWheel.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            self.imageWheel.isPagingEnabled = true
            self.numberStack.arrangedSubviews.forEach {
                $0.tintColor = UIColor(named: "diselectColor")
            }
            self.numberStack.arrangedSubviews[self.currentImageIndex - 1].tintColor = UIColor(named: "mainColor")
            self.numberStack.layoutIfNeeded()
            self.newsBodyLabel.text = self.newsItems[self.currentImageIndex - 1].0
            self.sourceLabel.text = self.newsItems[self.currentImageIndex - 1].1
        }
    }
    
    @objc func buttonClicked(view: UIButton) {
        let itemIndex = Int(view.currentTitle!)! - 1
        currentImageIndex = itemIndex
        let indexPath = IndexPath(item: itemIndex, section: 0)
        DispatchQueue.main.async {
            self.imageWheel.isPagingEnabled = false
            self.imageWheel.scrollToItem(at: indexPath, at: .right, animated: true)
            self.imageWheel.isPagingEnabled = true
            self.numberStack.arrangedSubviews.forEach {
                $0.tintColor = UIColor(named: "diselectColor")
            }
            view.tintColor = UIColor(named: "mainColor")
            self.numberStack.layoutIfNeeded()
            self.newsBodyLabel.text = self.newsItems[self.currentImageIndex].0
            self.sourceLabel.text = self.newsItems[self.currentImageIndex].1
        }
    }
}

extension ViewController: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let index = indexPath.item
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoWheelCell", for: indexPath) as! PhotoWheelCell
        cell.fill(image: UIImage(named: "image\(index + 1)")!)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: view.frame.height)
    }
}
